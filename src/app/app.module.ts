import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TextEditComponent } from './text-edit/text-edit-component';
import {FormsModule} from "@angular/forms";
import { LoginComponent } from './login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { AuthInterceptor } from './interceptors/AuthInterceptor';
import { ProjectComponent } from './project/project.component';
import { ChatComponent } from './chat/chat.component';

const appRoutes: Routes = [
  {path:"login", component:LoginComponent},
  {path:"socket", component:TextEditComponent},
  {path:"register", component:RegisterComponent},
  {path:"", component:HomeComponent},
  {path:"project/:userName/:projectName", component:ProjectComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    TextEditComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    ProjectComponent,
    ChatComponent,
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [ { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }],
  bootstrap: [AppComponent]
})

export class AppModule { }
