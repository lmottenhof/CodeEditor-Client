import { Injectable, OnDestroy } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AuthService } from '../services/AuthService';
import { endpoints } from 'src/environments/endpoints';
import { projection } from '@angular/core/src/render3';


@Injectable({
    providedIn: "root"
})

export class WebSocketConnectionManager implements OnDestroy {
    public static readonly chatChannel = 0;
    public static readonly textEditChannel = 1;

    private socket: WebSocket = undefined;
    private projectRoot : string;
    private url : string;

    constructor(private authService : AuthService) {
        const url: string = endpoints.socket;
        if (!(url.startsWith("ws://") || !url.startsWith("wss://"))) {
            throw "URL should start with 'ws://' or 'wss://'";
        }

        this.url = url; 
    }

    ngOnDestroy(): void {
        this.socket.close(1001);
    }

    public getConnection(channel: number, projectRoot : string) {
        if(this.socket === undefined) {
            this.connectToProject(projectRoot)
        }
        else if(this.projectRoot !== projectRoot) {
            this.socket.close(1001);
            this.connectToProject(projectRoot)
        }

        return new Channel(this.socket, channel)
    }

    private connectToProject(projectRoot : string) {
        this.projectRoot = projectRoot;
        this.socket = new WebSocket(this.url + "/" + projectRoot);
        const token : string = this.authService.getToken();
        if(token === undefined || token === null || token === "") {
            throw Error("Token is unset");
        }

        this.socket.onopen = () => this.socket.send(token);
        this.socket.binaryType = "arraybuffer";
    }
}

export interface WebSocketChannel {
    readonly handlerType: number;
    onMessage: (event: Uint8Array) => void;
    sendMessage(buffer: Uint8Array | string): void;
    addEventListener(listener: (data: Uint8Array) => void): void;
    removeEventListener(listener: (data: Uint8Array) => void): void;
}

class Channel implements WebSocketChannel {
    private readonly messageTypeIndex: number = 0;
    private readonly socket: WebSocket;
    private readonly listeners: ((event: Uint8Array) => void)[] = [];
    public readonly handlerType: number;
    public onMessage: ((event: Uint8Array) => void);

    public constructor(socket: WebSocket, handlerType: number) {
        this.socket = socket;
        this.handlerType = handlerType;
        this.socket.addEventListener("message", this.messageReceived.bind(this));
        this.socket.onerror = error => console.log(error);
    }

    private messageReceived(event: MessageEvent) {
        console.log(event)
        const buffer: Uint8Array = new Uint8Array(event.data)
        console.log(buffer)
        console.log(this.handlerType);
        if (buffer[this.messageTypeIndex] === this.handlerType) {
            if (this.onMessage !== undefined) {
                this.onMessage(buffer);
            }

            this.listeners.forEach((l) => l(buffer));
        }
    }

    public sendMessage(message: Uint8Array | string): void {
        console.log(message);

        const func = () => {
            this.socket.send(message);
            this.socket.removeEventListener("open", func);
        }

        if(this.socket.readyState !== this.socket.OPEN) {
            this.socket.addEventListener("open", func)
        }
        else {
            this.socket.send(message);
        }        
    }

    public addEventListener(listener: (data: Uint8Array) => void): void {
        this.listeners.push(listener);
    }

    public removeEventListener(listener: (data: Uint8Array) => void): void {
        if (this.listeners.indexOf(listener)) {
            this.listeners.splice(this.listeners.indexOf(listener), 1);
        }
    }
}
