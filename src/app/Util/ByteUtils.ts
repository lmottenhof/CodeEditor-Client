export class ByteUtil {
    public static bytesToNumber(buffer: Uint8Array, size: number, offset: number = 0): number {
        let result: number = 0;
        for (let i: number = 0; i < size; i++) {
          result |= buffer[offset + i] << ((size - 1 - i) * 8);
        }
        return result;
      }
    
      public static numberToBytes(num: number, size: number): Uint8Array  {
        let buffer: Uint8Array = new Uint8Array();
    
        for (let i: number = 0; i < size; i++) {
          buffer[i] = num >> ((size - 1 - i) * 8);
        }
        return buffer;
      }
    
      public static writeNumberToBuffer(num: number, buffer: Uint8Array, size: 1 | 2 | 3 | 4, offset: number): void {
        for (let i: number = 0; i < size; i++) {
          buffer[offset + i] = num >> ((size - 1 - i) * 8);
        }
      }

      public static writeStringToBuffer(str : string, buffer : Uint8Array, offset:number) : void {
        const bytes : Uint8Array = new TextEncoder().encode(str);
        buffer.set(bytes, offset);
      }

      public static readStringFromBuffer(buffer : Uint8Array, length : number, offset : number) : string {
        return new TextDecoder().decode(buffer.slice(offset, offset + length))
      }
}