import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../services/project.service';
import { Project } from '../models/Project';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  projects : Project[] = [];
  public newProjectName : string = ""

  constructor(private projectService : ProjectService, private router : Router) { }

  ngOnInit() {
    this.loadProjects();
  }

  public loadProjects() {
    this.projectService.getProjects().subscribe((projects : Project[]) => this.projects = projects);
  }

  newProject() : void {
    this.projectService.newProject(this.newProjectName).subscribe(_ => this.loadProjects())
  }

  loadProject(userName : string, projectName : string) {
    this.router.navigate(["/project/" + userName + "/" + projectName]);
  }

}
