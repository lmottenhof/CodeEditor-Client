import { Component, OnInit } from '@angular/core';
import { ChatService } from '../services/chat.service';
import { ChatMessage } from '../models/ChatMessage';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  public textBoxText : string;

  public messages : ChatMessage[] = [];

  constructor(private chatService : ChatService) { }

  ngOnInit() {
    this.chatService.addMessageReceivedListener(this.onMessage.bind(this));
  }

  public selectProject(projectRoot : string) {
    this.chatService.connect(projectRoot);
  }

public send() : void {
  console.log(this.textBoxText)
  this.chatService.sendMessage(this.textBoxText);
  this.textBoxText = "";
}

public onMessage(message : ChatMessage) : void{
  console.log(message);
  this.messages.push(message);
  console.log(this.messages);
}
}