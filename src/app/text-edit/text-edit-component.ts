import { Component, OnInit } from '@angular/core';
import { TextEditorModule } from '../text-editor/text-editor.module';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-text-edit',
  templateUrl: './text-edit.component.html',
  styleUrls: ['./text-edit.component.css'],
  providers: [TextEditorModule]
})
export class TextEditComponent implements OnInit {
  
  public get text() {
    return this.textEditor.text;
  }

  public set text(value: String) {
    this.textEditor.text = value;
  }

  public constructor(private textEditor : TextEditorModule) {
    
  }

  public selectProject(projectRoot : string) {
    this.textEditor.selectProject(projectRoot);
  }

  ngOnInit() {

  }
  
  //As of the time of writing, definitions for the InputEvent class are not available.
  public onTextChange(event: any) {
    console.log(event);
    let target: HTMLInputElement = (event.target as HTMLInputElement);
    let position: number = target.selectionStart;
    let endPosition: number = target.selectionEnd;
    switch (event.inputType) {
      case "insertText":
      case "insertFromPaste":
        position = position - event.data.length;
        this.textEditor.addText(event.data, position);
        break;
      case "deleteContentBackward":
      case "deleteContentForward":
      case "deleteByCut":
        this.textEditor.removeText(position)
        break;
      case "insertLineBreak":
        this.textEditor.addText(String.fromCharCode(10), position - 1)
        break;
      case "historyUndo":
      case "historyRedo":
        let lengthDiff: number = this.text.length - this.textEditor.lastLength;
        if (this.textEditor.lastLength < this.text.length) {
          if (position !== endPosition) {
            this.textEditor.addText(this.text.slice(position, endPosition), position)
          }
          else {
            this.textEditor.addText(this.text.slice(position - lengthDiff, position), position)
          }
          
        } else if (this.textEditor.lastLength > this.text.length) {
          this.textEditor.removeText(position);
        }
        break;
      default:
        throw "Action not supported: " + event.inputType;
        break;
    }
  }

  public onKeyPress(event: KeyboardEvent) {
    let target: HTMLInputElement = (event.target as HTMLInputElement);
    if (target.selectionStart !== target.selectionEnd && event.key !== "Backspace") {
      this.textEditor.removeText(target.selectionStart);
    }
  }

  public onKeyDown(event: KeyboardEvent) {
    console.log(event);
    if (event.key === "Tab") {
      event.preventDefault();
      const space: String = String.fromCharCode(32);
      const target: HTMLTextAreaElement = (event.target as HTMLTextAreaElement);
      const position: number = target.selectionStart;
      target.focus();
      document.execCommand("insertText", false, space.repeat(4))
      if (this.text[position + 1] !== space) {
        this.text = this.text.slice(0, position) + space.repeat(2) + this.text.slice(position);
        this.textEditor.addText(space.repeat(2), position);
      }
    }
  }

  public cancel(event: MouseEvent) {
    let target: HTMLInputElement = (event.target as HTMLInputElement);
    if (target.id === "editor" && (event.shiftKey || event.ctrlKey)) {
      event.preventDefault();
    }
    
  }

  public openFile(file : string, projectName : string, userName : string) {
    this.textEditor.openFile(file, projectName, userName).subscribe(() => {

    });
  }
}