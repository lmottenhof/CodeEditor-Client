import { NgModule, OnDestroy, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Injectable } from '@angular/core';
import { WebSocketConnectionManager, WebSocketChannel } from '../web-socket-handler/WebSocketConnectionManager';
import { ByteUtil } from '../Util/ByteUtils';
import { ProjectService } from '../services/project.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

enum editType {
    add = 0,
    remove = 1
}

@Injectable({
    providedIn: "root"
})

@NgModule({
    declarations: [],
    imports: [
        CommonModule
    ]
})

export class TextEditorModule implements OnDestroy {
    //Type of the binary message. 1 for editting messages;
    public readonly messageType: number;
    //Index of the type of edit.
    private readonly editTypeIndex: number = 1;
    //Index of the changed text position.
    private readonly positionIndexStart : number = 2;
    //Number of bytes reserved for the position. 
    private readonly positionLength = 4;
    //Index of the added text for when editType=add.
    private readonly messageTextIndex: number = 6;
    //Amount of removed characters for when editType=remove.
    private readonly removeLengthIndex : number = 6;

    private readonly messageTypeIndex : number = 0;

    private _lastLength: number;

    private channel : WebSocketChannel;

    public get lastLength() {
        return this._lastLength;
    }

    ngOnDestroy(): void {
        this.channel.removeEventListener(this.onMessage)
    }
    private _text: String = "";

    public get text() {
        return this._text;
    }

    public set text(value: String) {
        this._text = value;
        

    }

    public constructor(private manager: WebSocketConnectionManager, private projectService : ProjectService) {
        //super(manager.getConnection(WebSocketConnectionManager.textEditChannel));
        this.messageType = WebSocketConnectionManager.textEditChannel;
        this.onMessage = this.onMessage.bind(this);
        
    }

    public selectProject(projectRoot : string) {
        this.channel = this.manager.getConnection(WebSocketConnectionManager.textEditChannel, projectRoot);
        this.channel.addEventListener(this.onMessage.bind(this))
    }

    public removeText(position: number) {
        let buffer: Uint8Array = new Uint8Array(4 + this.positionLength);
        buffer[this.messageTypeIndex] = this.messageType;
        buffer[this.editTypeIndex] = editType.remove;

        ByteUtil.writeNumberToBuffer(position, buffer, this.positionLength, this.positionIndexStart)
        console.log(this.text.length);
        console.log(this._lastLength);
        buffer[this.removeLengthIndex] = this.lastLength - this.text.length;

        this.channel.sendMessage(buffer);
        this._lastLength = this.text.length;
    }

    public addText(text: String, position: number): void {
        let buffer: Uint8Array = new Uint8Array(2 + this.positionLength + text.length);
        buffer[0] = 1;
        buffer[this.editTypeIndex] = editType.add;
        ByteUtil.writeNumberToBuffer(position, buffer, this.positionLength, this.positionIndexStart);

        for (let i: number = 0; i < text.length; i++) {
            buffer[this.messageTextIndex + i] = text.charCodeAt(i);
        }

        console.log(buffer);
        this.channel.sendMessage(buffer);
        this._lastLength = this.text.length;
    }

    public onMessage(data: Uint8Array): void {
        console.log(data);
        const pos: number = ByteUtil.bytesToNumber(data, this.positionLength, this.positionIndexStart);
        switch (data[this.editTypeIndex]) {
            case editType.add:
                let addedText: String = new TextDecoder().decode(data.slice(this.messageTextIndex));
                this._text = this._text.slice(0, pos) + addedText + this._text.slice(pos);
                break;
            case editType.remove:
                const tmp : string[] = this._text.split('');
                tmp.splice(pos, data[this.removeLengthIndex]);
                this._text = tmp.join('');
                break;
        }
        this._lastLength = this.text.length;
    }

    public openFile(file : string, projectName : string, userName : string) : Observable<string>{
        return this.projectService.getFile(file, projectName, userName).pipe(map((fileContent : string) => {
            const buffer : Uint8Array = new Uint8Array(2 + file.length); 
            buffer[0] = this.messageType;
            buffer[1] = 2;
            ByteUtil.writeStringToBuffer(file, buffer, 2);
            this.channel.sendMessage(buffer);
            this._text=fileContent;
            this._lastLength=this._text.length;
            return fileContent;
        }))
        
    }
}