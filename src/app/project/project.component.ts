import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ProjectService } from '../services/project.service';
import { Project } from '../models/Project';
import { ProjectDetail } from '../models/ProjectDetail';
import { TextEditComponent } from '../text-edit/text-edit-component';
import { ChatComponent } from '../chat/chat.component';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {
@ViewChild(TextEditComponent) textEditComponent : TextEditComponent;
@ViewChild(ChatComponent) chatComponent : ChatComponent;

  project : ProjectDetail;
  currentFileString = "";
  loaded : boolean = false;
  file : FileList;
  uploadFileName : string;

  constructor(private route : ActivatedRoute, private projectService : ProjectService) { 
  }

  ngOnInit() {
    this.route.paramMap.subscribe((params : ParamMap) => {
      this.projectService.getProject(params.get("projectName"), params.get("userName")).subscribe(p => {
        this.project = p;
        this.loaded = true;
        this.textEditComponent.selectProject(this.project.projectRoot);
        this.chatComponent.selectProject(this.project.projectRoot);
      }
    )});
  }

  public loadProject(projectName : string, userName : string)  : void{
    this.projectService.getProject(projectName, userName).subscribe(p => {
      this.project = p;
      this.loaded = true;
      this.textEditComponent.selectProject(this.project.projectRoot);
      this.chatComponent.selectProject(this.project.projectRoot);
    });
  }

  loadFile(path : string) {
    const projectDetails : string[] = this.project.projectRoot.split("/"); 
    this.textEditComponent.openFile(path, projectDetails[1], projectDetails[0]);
  }

  uploadFile() {
    const projectDetails = this.project.projectRoot.split("/");
    this.projectService.uploadFile(this.file, this.uploadFileName, this.project.projectRoot).subscribe(_ => this.loadProject(projectDetails[1], projectDetails[0]));
  }

  //Definitions for this event are not known
  public selectFile(event : any) {
    console.log((event))
    this.file = event.target.files;
  }
}
