import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/AuthService';
import { Router } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email : string;
  password : string;
  constructor(private authService : AuthService, private router : Router) { }


  onSubmit() { 
    this.authService.login(this.email, this.password).subscribe(resp => {
      if(resp.ok) {
        this.router.navigate(["/"])
      }
      else {
        console.log(resp.status);
      }
    },error => console.log(error));
  }
 
  ngOnInit() { 

  }

}
