import {ProjectFile} from "./ProjectFile";

export class ProjectDetail {
    name : string;
    projectRoot : string;
    files: ProjectFile[];;
}