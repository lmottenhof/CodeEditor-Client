export class ChatMessage {
    sender : string;
    content : string;
    time : Date;
}