import { NgModule, OnDestroy, Component } from '@angular/core';
import { Injectable } from '@angular/core';
import { WebSocketConnectionManager, WebSocketChannel } from '../web-socket-handler/WebSocketConnectionManager';
import { CommonModule } from '@angular/common';
import { ByteUtil } from '../Util/ByteUtils';
import { ChatMessage } from '../models/ChatMessage';

@Injectable({
  providedIn: "root"
})

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class ChatService implements OnDestroy {
  
  private readonly messageType : number;
  private listeners : ((message : ChatMessage) => void)[] = [];
  private channel : WebSocketChannel;

  public constructor(private manager: WebSocketConnectionManager,) {
    //super(manager.getConnection(WebSocketConnectionManager.chatChannel));
    this.messageType = WebSocketConnectionManager.chatChannel;
    
  }
  private onMessage(data : Uint8Array) : void {
    const message : ChatMessage = new ChatMessage();
    const userNameLength : number = data[2];
    const messageLength : number = data[3 + userNameLength];
    message.sender = ByteUtil.readStringFromBuffer(data, userNameLength, 3);
    message.content = ByteUtil.readStringFromBuffer(data, messageLength, 4 + userNameLength);
    message.time = new Date();
    console.log(message)
    this.listeners.forEach(l => l(message));
  }

  ngOnDestroy(): void {
    this.channel.removeEventListener(this.onMessage);
  }

  public addMessageReceivedListener(listener : (message : ChatMessage) => void) : void {
    this.listeners.push(listener);
  }

  public sendMessage(content : string) : void {
    const buffer : Uint8Array = new Uint8Array(content.length + 3);
    buffer[0] = this.messageType;
    buffer[1] = 0;
    buffer[2] = content.length;
    ByteUtil.writeStringToBuffer(content, buffer, 3);
    this.channel.sendMessage(buffer);
  }

  public connect(project : string) {
    console.log(project);
    this.channel = this.manager.getConnection(WebSocketConnectionManager.chatChannel, project);
    this.channel.addEventListener(this.onMessage.bind(this));
    this.channel.sendMessage(project);
  }
}