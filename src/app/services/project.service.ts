import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './AuthService';
import { endpoints } from 'src/environments/endpoints';
import { Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { ProjectDetail } from '../models/ProjectDetail';
import { Project } from '../models/Project';

const headers = new HttpHeaders()
  .set("Content-Type", "application/json")
  .set('Access-Control-Allow-Origin', '*');

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private httpClient: HttpClient, private authService: AuthService) { }

  public getProjects(): Observable<Project[]> {
    return this.httpClient.get<Project[]>(endpoints.project.base, { headers: headers, observe: 'response' }).pipe(map(resp => {
      return resp.body;
    }));
  }

  public newProject(name: string): Observable<{ id: string, name: string, username: string }> {
    return this.httpClient.post<{ id: string, name: string, username: string }>(endpoints.project.new, { projectName: name }, { headers: headers, observe: 'response' }).pipe(map(resp => {
      if (resp.ok) {
        return resp.body;
      } else {
        throw throwError(resp);
      }
    }))
  }

  public getProject(projectName: string, userName: string): Observable<ProjectDetail> {
    return this.httpClient.get<ProjectDetail>(endpoints.project.base + "/" + userName + "/" + projectName, { headers: headers, observe: 'response' }).pipe(map(resp => {
      if (resp.ok) {
        return resp.body;
      } else {
        throw throwError(resp);
      }
    }));
  }

  public getFile(fileName: string, projectName: string, userName: string): Observable<string> {
    return this.httpClient.get<{ content: string }>(endpoints.project.base + "/" + userName + "/" + projectName + "/" + fileName, 
    { headers: headers, observe: 'response' }).pipe(map(resp => {
      if (resp.ok) {
        return resp.body.content;
      } else {
        throw throwError(resp);
      }
    }))
  };


  public uploadFile(file : FileList, path : string, projectRoot : string) : Observable<void>{
    const formData : FormData = new FormData();
    formData.append("file", file[0], file[0].name)
   
    return this.httpClient.post<void>(endpoints.project.base + "/" + projectRoot + "/" + path, formData,
    { headers: headers.set("Content-Type","multipart/form-data"), observe: 'response' }).pipe(map(resp => {
      if (resp.ok) {
        
      } else {
        throw throwError(resp);
      }
    }))
  }
}