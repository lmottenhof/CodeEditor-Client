import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { endpoints } from 'src/environments/endpoints';
import { Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { map } from "rxjs/operators"
import { Router } from '@angular/router';


const headers = new HttpHeaders()
    .set("Content-Type", "application/json")
    .set('Access-Control-Allow-Origin', '*');

@Injectable({
    providedIn: "root"
})
export class AuthService {
    constructor(private client: HttpClient, private router : Router) {

    }

    public register(email: string, name: string, password: string): Observable<void> {
        return this.client.post(endpoints.user.register, {
            email: email,
            name: name,
            password: password
        }, { headers: headers, observe: 'response' }).pipe(map(resp => {
            if (!resp.ok) {
                throw throwError(resp);
            }
        }));
    }

    public login(email: string, password: string): Observable<HttpResponse<{ token: string }>> {
        return this.client.post<{ token: string }>(endpoints.user.login, {
            email: email,
            password: password
        }, { headers: headers, observe: 'response' }).pipe(map((resp: HttpResponse<{ token: string }>) => {
            if (resp.ok) {
                document.cookie = `token=${resp.body.token}`;
                setTimeout(() => this.router.navigate(['/login']), 60*60*2*1000)
            }
            else {
                //throw throwError(resp);
            }
            return resp;
        }));
    }

    public getToken(): string {
        const value: string = "; " + document.cookie;
        const parts: string[] = value.split("; " + "token" + "=");
        if (parts.length == 2) return parts.pop().split(";").shift();
    }
}