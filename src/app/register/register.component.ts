import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/AuthService';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  email : string;
  username : string;
  password : string;
  confirmPassword : string;

  constructor(private authService : AuthService, private router : Router) { }

  onSubmit() : void {
    if(this.password === this.confirmPassword) {
      this.authService.register(this.email, this.username, this.password)
      .subscribe(resp => {
        this.router.navigate(["/login"]);
      },
      error => console.log(error));
    }
  }

  ngOnInit() {
  }

}
