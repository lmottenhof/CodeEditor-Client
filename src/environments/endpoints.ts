import { environment } from './environment'

export const endpoints = {
  user : {
    register: environment.restServerURL + "/user/register",
    login: environment.restServerURL + "/user/login"
  },

  project : {
    base : environment.restServerURL+"/project",
    new: environment.restServerURL+"/project/new"
  },

    socket : environment.socketURL + "/socket",
  }